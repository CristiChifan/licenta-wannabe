using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BansheeGz.BGSpline.Curve;
using UnityEngine.SceneManagement;
public enum TileType
{
    Empty, 
    Enemy,
    EpicEnemy,
    Rest,
    Recruit,
    Treasure,
    Boss
}


public class Tile : MonoBehaviour
{
    [SerializeField] private TileType _type = TileType.Empty;
    [SerializeField] private Material _hoverMaterial, _defaultMaterial;
    [SerializeField] private Color _disabledColor;
    [SerializeField] private bool _isAvailable = false;
    private GridManager _manager;
    private SpriteRenderer _renderer, _markerRenderer;
    public KeyValuePair<int, int> _coords;
    public BGCurvePoint _point;
    private bool _isVisied = false, _isCurrent = false;

    private void Start()
    {
        _manager = GameObject.Find("GridManager").GetComponent<GridManager>();


        //get renderers
        _renderer = GetComponent<SpriteRenderer>();
        _markerRenderer = this.transform.Find("Marker").GetComponent<SpriteRenderer>();

        GenerateRandomSpriteTile();
    }

    #region getter&setters
    public KeyValuePair<int, int> Coords { get { return _coords; } set { _coords = value; } }
    public bool IsAvailable { get { return _isAvailable; } set { _isAvailable = value; } }
    public bool IsCurrent { get { return _isCurrent; } set { _isCurrent = value; } }
    public bool IsVisied { get { return _isVisied; } set { _isVisied = value; } }
    public BGCurvePoint Point { get { return _point; } set { _point = value; } }
    public TileType TileType { get { return _type; } set { _type = value; } }

    #endregion

    #region methods
    public void Init(KeyValuePair<int, int> coords, BGCurvePoint point)
    {
        this.name = $"tile_{coords.Key}_{coords.Value}";
        this.Coords = coords;
        GetComponent<Animator>().enabled = false;
        this.transform.Find("Marker").localScale = Vector2.zero;
        this._point = point;

    }

    public void GenerateRandomSpriteTile()
    {
        // 9 different sprites
        int random = UnityEngine.Random.Range(0, 8);
        Sprite[] sprites = Resources.LoadAll<Sprite>("Sprites/Tiles/tiles_forest_conifer_sparse_clear_green");
        _renderer.sprite = sprites[random];


        //always start with a battle
        if (_coords.Key == 0)
        {
            _type = TileType.Enemy;

        } 
        else if (_coords.Key != _manager.Size * 2 - 2)
        {
            // decide the type of sprite
            _type = (TileType)UnityEngine.Random.Range((int)TileType.Empty, (int)TileType.Treasure + 1);

        } 
        else
        {
            if (Coords.Value == _manager.Size / 2)
            {
                _type = TileType.Boss;
            }
        }
        this.transform.Find(Enum.GetName(typeof(TileType), _type)).GetComponent<SpriteRenderer>().enabled = true;

    }


    public void ApplyCurrentMarker()
    {
        _markerRenderer.enabled = true;
        _renderer.color = Color.white;
        this.transform.Find("Marker").transform.LeanScale(new Vector3(1.34f, 1.34f, 0), 0.8f).setEaseOutBack();
    }
    public void ApplyVisitedMarker()
    {
        _markerRenderer.sprite = Resources.Load<Sprite>("Sprites/Markers/overlay_marker_standard_cross");
    }

    public void Disable()
    {
        if (!_isCurrent)
        {
            _isAvailable = false;
            _renderer.color = _disabledColor;
            this.transform.Find(Enum.GetName(typeof(TileType), _type)).GetComponent<SpriteRenderer>().color = _disabledColor;

        }
    }


    #endregion 

    #region events
    public void OnMouseEnter()
    {
        if (_isAvailable && _isVisied == false)
        {
            if (!_isCurrent)
            {
                this.GetComponent<Renderer>().sharedMaterial = _hoverMaterial;
            }
        }
    }

    public void OnMouseExit()
    {
        if (_isAvailable && _isVisied == false)
        {
            if (!_isCurrent)
            {
                this.GetComponent<Renderer>().sharedMaterial = _defaultMaterial;
                _renderer.color = Color.white;
            }
        }
    }


    public void OnMouseDown()
    {
        if (_isAvailable)
        {
            ApplyCurrentMarker();
            _manager.DecideCurrent(this);

        }
    }

    #endregion
    

}
