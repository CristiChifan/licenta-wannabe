using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TreasureController : MonoBehaviour
{
    private PlayerHandler _playerHandler;

    private static int TREASURE_SCENE_INDEX = 5;

    [SerializeField] private Item _item;

    // Start is called before the first frame update
    void Start()
    {
        _playerHandler = GameObject.Find("PlayerHandler").GetComponent<PlayerHandler>();
        SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(TREASURE_SCENE_INDEX));
        SetupCharacters();
    }

    private void SetupCharacters()
    {
        for (int i = 0; i < _playerHandler.Characters.Count; i++)
        {
            if (i < 2)
            {
                var _clone = Instantiate(_playerHandler.Characters[i], new Vector3(-1f * ((i + 1) * 2), -2f, 221.3647f), Quaternion.identity);
                _clone.gameObject.SetActive(true);
            }
            else
            {
                var _clone = Instantiate(_playerHandler.Characters[i], new Vector3(1f * ((i - 1) * 2), -2f, 221.3647f), Quaternion.identity);
                _clone.GetComponent<SpriteRenderer>().flipX = false;
                _clone.gameObject.SetActive(true);
            }
        }
    }



    public void CreateRandomItem()
    {
        Item item = Instantiate(_item, new Vector3(0, 0.8f, 0), Quaternion.identity);
        item.transform.localScale = new Vector3(8,8,0);
        item.ChooseRandomEffect(_playerHandler.Characters);
        _playerHandler.UpdateItemUI(item.GetComponent<SpriteRenderer>().sprite);
    }
}
