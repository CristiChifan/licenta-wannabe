using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Enemy : MonoBehaviour
{
    // Start is called before the first frame update
    private float _hp = 100f, _speed;
    private FightHandler _fightHandler;
    public int damage = 10;
    public float MaxHealth = 100f;
    public Character targetCharacter;

    public float Health
    {
        get { return _hp; }
        set
        {
            _hp = value;
            OnHealthChange?.Invoke(Health / MaxHealth);
        }
    }

    public UnityEvent<float> OnHealthChange;


    void Start()
    {
        _fightHandler = GameObject.Find("FightHandler").GetComponent<FightHandler>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnMouseDown()
    {
        if (Health > 0)
        {
            _fightHandler.chosenEnemy = this;
            foreach (Enemy enemy in _fightHandler.enemies)
            {
                enemy.GetComponent<Renderer>().sharedMaterial = _fightHandler._defaultMaterial;
            }
            GetComponent<Renderer>().sharedMaterial = _fightHandler._activeMaterial;
        }
    }

    public void DecideTargetCharacter()
    {
        var characters = _fightHandler.Characters;
        int characterIndex = Random.Range(0, characters.Count);
        targetCharacter = characters[characterIndex];
        Debug.Log(characterIndex);
    }
}
