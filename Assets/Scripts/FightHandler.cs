using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class FightHandler : MonoBehaviour
{
    private PlayerHandler _playerHandler;
    private LevelLoader _levelLoader;
    private static int FIGHT_SCENE_INDEX = 2;
    private Character activeCharacter;
    public bool isPlayerTurn = true;
    private int turnCounter = 0;
    public List<Character> Characters = new List<Character>();
    private List<Character> OriginalCharacters = new List<Character>();
    public Enemy chosenEnemy;
    public List<Enemy> enemies = new List<Enemy>();
    private bool exitedFight = false;
    public TileType tileType;
    private GridManager _gridManager;

    [SerializeField] public Sprite bigEnemySprite;
    [SerializeField] public Sprite bossSprite;
    [SerializeField] public Enemy _enemyPrefab;
    [SerializeField] public Material _activeMaterial;
    [SerializeField] public Material _defaultMaterial;
    [SerializeField] private Button runButton;
    [SerializeField] private Button firstButton;
    [SerializeField] private Button secondButton;
    [SerializeField] private Button thirdButton;
    [SerializeField] private Button fourthButton;
    [SerializeField] private Canvas gameOverScreen;


    void Start()
    {
        _playerHandler = GameObject.Find("PlayerHandler").GetComponent<PlayerHandler>();
        _levelLoader = GameObject.Find("LevelLoader").GetComponent<LevelLoader>();
        _gridManager = GameObject.Find("GridManager").GetComponent<GridManager>();
        SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(FIGHT_SCENE_INDEX));
        tileType = _playerHandler.currentFightTileType;
        if (tileType == TileType.Boss)
        {
            runButton.gameObject.SetActive(false);
        }
        SetupFriendlySide();
        SetupEnemySide();
        chosenEnemy = enemies[0];
        chosenEnemy.GetComponent<Renderer>().sharedMaterial = _activeMaterial;
    }

    void SetupFriendlySide()
    {
        for (int i = 0; i < _playerHandler.Characters.Count; i++)
        {
            if (_playerHandler.Characters[i].Health > 0)
            {
                var _clone = Instantiate(_playerHandler.Characters[i], new Vector3(-1f * ((i + 1) * 2), -2f, 221.3647f), Quaternion.identity);
                _clone.Health = _playerHandler.Characters[i].Health;
                _clone.Speed = _playerHandler.Characters[i].Speed;
                _clone.Abilities = _playerHandler.Characters[i].Abilities;
                _clone.Name = _playerHandler.Characters[i].Name;
                Characters.Add(_clone);
                OriginalCharacters.Add(_clone);
                _clone.gameObject.SetActive(true);
            }
        }
        Characters.Sort((a, b) => { return a.Speed.CompareTo(b.Speed); });
    }

    void SetupEnemySide()
    {
        switch (tileType)
        {
            case TileType.Enemy:
                for (int i = 0; i < _playerHandler.Characters.Count; i++)
                {
                    var _clone = Instantiate(_enemyPrefab, new Vector3(1f * ((i + 1) * 2), -2f, 221.3647f), Quaternion.identity);
                    _clone.GetComponent<SpriteRenderer>().sortingOrder = 10;
                    _clone.GetComponentInChildren<Canvas>().sortingOrder = 10;
                    enemies.Add(_clone);
                }
                break;

            case TileType.EpicEnemy:
                for (int i = 0; i < _playerHandler.Characters.Count; i++)
                {
                    var _cloneBig = Instantiate(_enemyPrefab, new Vector3(1f * ((i + 1) * 2), -2f, 221.3647f), Quaternion.identity);
                    _cloneBig.GetComponent<SpriteRenderer>().sortingOrder = 10;
                    _cloneBig.GetComponentInChildren<Canvas>().sortingOrder = 10;
                    _cloneBig.GetComponent<SpriteRenderer>().sprite = bigEnemySprite;
                    _cloneBig.damage = 15;
                    _cloneBig.Health *= 1.5f;
                    _cloneBig.MaxHealth *= 1.5f;
                    enemies.Add(_cloneBig);
                }
                break;

            case TileType.Boss:
                var _cloneBoss = Instantiate(_enemyPrefab, new Vector3(1f * ((1 + 1) * 2), -2f, 221.3647f), Quaternion.identity);
                _cloneBoss.GetComponent<SpriteRenderer>().sortingOrder = 10;
                _cloneBoss.GetComponentInChildren<Canvas>().sortingOrder = 10;
                _cloneBoss.GetComponent<SpriteRenderer>().sprite = bossSprite;
                _cloneBoss.GetComponent<Transform>().localScale = new Vector3(2, 2, 1);
                _cloneBoss.damage = 20;
                _cloneBoss.Health *= 2;
                _cloneBoss.MaxHealth *= 2;
                enemies.Add(_cloneBoss);
                break;

        }
        
    }

    void DecideActiveCharacter()
    {
        activeCharacter = Characters[turnCounter];
        activeCharacter.GetComponent<Renderer>().sharedMaterial = _activeMaterial;
        foreach (Character character in Characters)
        {
            if (character != activeCharacter)
            {
                character.GetComponent<Renderer>().sharedMaterial = _defaultMaterial;
            }
        }
    }

    void ChangeButtonText()
    {
        var firstButtonText = firstButton.GetComponentInChildren<TextMeshProUGUI>();

        firstButtonText.text = activeCharacter.Abilities[0].Name;

        var secondButtonText = secondButton.GetComponentInChildren<TextMeshProUGUI>();

        secondButtonText.text = activeCharacter.Abilities[1].Name;

        var thirdButtonText = thirdButton.GetComponentInChildren<TextMeshProUGUI>();

        thirdButtonText.text = activeCharacter.Abilities[2].Name;

        var fourthButtonText = fourthButton.GetComponentInChildren<TextMeshProUGUI>();

        fourthButtonText.text = activeCharacter.Abilities[3].Name;
    }

    public void IncrementTurn()
    {
        int charCount = Characters.Count;
        turnCounter++;
        if (turnCounter == charCount)
        {
            turnCounter = 0;
        }
    }

    public void CastAbility(Ability ability)
    {
        float modifier;
        switch (ability.DamageType)
        {
            case DamageType.Phisycal:
                modifier = activeCharacter.PhysicalDamage;
                break;
            case DamageType.Magical:
                modifier = activeCharacter.MagicDamage;
                break;
            case DamageType.Sustain:
                modifier = 0;
                break;
            default:
                modifier = 0;
                break;
        }


        if (ability.Target == Target.AllEnemies)
        {
            enemies.ForEach(enemy => enemy.Health -= ability.Effect + modifier);
        } 
        else if (ability.Target == Target.AllAllies)
        {
            if (ability.isShield) 
            {
                Characters.ForEach(ally => ally.Shield += ability.Effect + modifier);
                _playerHandler.Characters.ForEach(ally => ally.Shield += ability.Effect + modifier);
            }
            else
            {
                Characters.ForEach(ally => ally.Health += ability.Effect + modifier);
                _playerHandler.Characters.ForEach(ally => ally.Health += ability.Effect + modifier);

            }
        }
        else if (ability.Target == Target.Self)
        {
            if (ability.isShield)
            {
                activeCharacter.Shield += ability.Effect + modifier;
            }
            else
            {
                activeCharacter.Health += ability.Effect + modifier;
                _playerHandler.Characters[OriginalCharacters.IndexOf(activeCharacter)].Health += ability.Effect + modifier;
            }
        }
        else if (ability.Target == Target.SingleEnemy)
        {
            chosenEnemy.Health -= ability.Effect + modifier;
        }
    }


    public void CastAbility(int index)
    {
        if (isPlayerTurn)
        {
            CastAbility(activeCharacter.Abilities[index]);
            IncrementTurn();
            isPlayerTurn = false;
            if (chosenEnemy.Health <= 0)
            {
                enemies.Remove(chosenEnemy);
                chosenEnemy.GetComponent<SpriteRenderer>().sharedMaterial = _defaultMaterial;
                if (enemies.Count > 0)
                {
                    chosenEnemy = enemies[0];
                }
                chosenEnemy.GetComponent<SpriteRenderer>().sharedMaterial = _activeMaterial;
            }
        }
    }

    public void EnemyTurn()
    {
        if (!isPlayerTurn && enemies.Count > 0)
        {
            var activeEnemy = enemies[Random.Range(0, enemies.Count)];
            activeEnemy.DecideTargetCharacter();
            var targetCharacter = activeEnemy.targetCharacter;
            _playerHandler.Characters[OriginalCharacters.IndexOf(targetCharacter)].Health -= activeEnemy.damage;
            if (targetCharacter.Health <= 0)
            {
                Characters.Remove(targetCharacter);
                turnCounter = 0;
                Debug.Log("amscos");
            }
            targetCharacter.Health -= activeEnemy.damage;
            isPlayerTurn = true;
            //startPauseBetwwenTurns(1f);
        }
    }


    public bool DecideIfFightIsOver()
    {
        bool isOver = true;
        foreach(Enemy enemy in enemies)
        {
            if (enemy.Health > 0)
            {
                isOver = false;
            }
        }
        return isOver;
    }

    public void startPauseBetwwenTurns(float seconds)
    {
        StartCoroutine(PauseBetweenTurns(seconds));
    }
    public IEnumerator PauseBetweenTurns(float seconds)
    {
        Time.timeScale = 0.1f;
        yield return new WaitForSeconds(seconds);
        Time.timeScale = 1f;
    }

    // Update is called once per frame
    void Update()
    {
        if (Characters.Count == 0)
        {
            gameOverScreen.gameObject.SetActive(true);
        } 
        else
        {
            DecideActiveCharacter();
            ChangeButtonText();
            if (DecideIfFightIsOver() && !exitedFight)
            {
                _playerHandler.UpdateCharacterUI();
                _levelLoader.ReturnToMapFromFight();
                exitedFight = true;
                if (tileType == TileType.Boss)
                {
                    _gridManager._goodGameOverOverlay.gameObject.SetActive(true);
                }
            }
            EnemyTurn();

        }
    }
}
