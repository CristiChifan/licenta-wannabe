using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] Camera cam;
    [SerializeField] float zoom = 1f;
    [SerializeField] float moveAmount = 0.1f;
    float startingSize;
    private GridManager gridManager;
    private float mapMaxX, mapMinX, mapMaxY, mapMinY;
    [SerializeField] private SpriteRenderer mapRenderer;
    private Vector3 dragOrigin;

    private void Start()
    {
        gridManager = GameObject.Find("GridManager").GetComponent<GridManager>();

    }


    void Update()
    {
        if (cam.orthographic)
        {
            float changeAmount = Input.GetAxis("Mouse ScrollWheel") * zoom;
            float finalValue = cam.orthographicSize - changeAmount;

            if (finalValue < gridManager._size + 1 && finalValue > 0.3f)
            {
                cam.orthographicSize = finalValue;
                cam.transform.position = clampCamera(cam.transform.position);
            }
        }
        else
        {
            cam.fieldOfView -= Input.GetAxis("Mouse ScrollWheel") * zoom;
        }

        panCamera();
    }

    private void panCamera()
    {
        if (Input.GetMouseButtonDown(0))
        {
            dragOrigin = cam.ScreenToWorldPoint(Input.mousePosition);
        }
        if (Input.GetMouseButton(0))
        {
            Vector3 difference = dragOrigin - cam.ScreenToWorldPoint(Input.mousePosition);
            cam.transform.position = clampCamera(cam.transform.position + difference);
        }
    }

    private Vector3 clampCamera(Vector3 targetPosition)
    {

        mapMinX = mapRenderer.transform.position.x - mapRenderer.bounds.size.x / 2.2f;
        mapMaxX = mapRenderer.transform.position.x + mapRenderer.bounds.size.x / 2.2f;
        mapMinY = mapRenderer.transform.position.y - mapRenderer.bounds.size.y / 2.2f;
        mapMaxY = mapRenderer.transform.position.y + mapRenderer.bounds.size.y / 2.2f;

        float camHeight = cam.orthographicSize;
        float camWidth = cam.orthographicSize * cam.aspect;

        float minX = mapMinX + camWidth;
        float maxX = mapMaxX - camWidth;
        float minY = mapMinY + camHeight;
        float maxY = mapMaxY - camHeight;

        float newX = Mathf.Clamp(targetPosition.x, minX, maxX);
        float newY = Mathf.Clamp(targetPosition.y, minY, maxY);

        return new Vector3(newX, newY, targetPosition.z);

    }
}
