using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RestController : MonoBehaviour
{

    private PlayerHandler _playerHandler;

    private static int REST_SCENE_INDEX = 3;

    // Start is called before the first frame update
    void Start()
    {
        _playerHandler = GameObject.Find("PlayerHandler").GetComponent<PlayerHandler>();
        SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(REST_SCENE_INDEX));
        SetupCharacters();
    }



    private void SetupCharacters()
    {
        for (int i = 0; i < _playerHandler.Characters.Count; i++)
        {
            if (i < 2)
            {
                var _clone = Instantiate(_playerHandler.Characters[i], new Vector3(-1f * ((i + 1) * 2), -2f, 221.3647f), Quaternion.identity);
                _clone.gameObject.SetActive(true);
            }
            else
            {
                var _clone = Instantiate(_playerHandler.Characters[i], new Vector3(1f * ((i - 1) * 2), -2f, 221.3647f), Quaternion.identity);
                _clone.GetComponent<SpriteRenderer>().flipX = false;
                _clone.gameObject.SetActive(true);
            }
        }
    }

    public void RestoreHp()
    {
        foreach(Character character in _playerHandler.Characters)
        {
            character.Health = character.MaxHealth;
        }
        _playerHandler.UpdateCharacterUI();
    }
}
