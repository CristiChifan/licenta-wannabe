using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ShowCharacterInfo : MonoBehaviour
{
    // Start is called before the first frame update
    public void ShowName(TextMeshProUGUI name)
    {
        LeanTween.scale(name.gameObject, Vector3.one, 0.5f).setEaseInOutBack();
    }

    public void HideName(TextMeshProUGUI name)
    {
        LeanTween.scale(name.gameObject, Vector3.zero, 0.5f).setEaseInOutBack();
    }

    public void ShowSBar(RectTransform sbar)
    {
        LeanTween.scale(sbar.gameObject, Vector3.one, 0.5f);
    }

    public void ShowHBar(RectTransform hbar)
    {
        LeanTween.scale(hbar.gameObject, Vector3.one, 0.5f);
    }


    public void HideSBar(RectTransform sbar)
    {
        LeanTween.scale(sbar.gameObject, Vector3.zero, 0.5f);
    }

    public void HideHBar(RectTransform hbar)
    {
        LeanTween.scale(hbar.gameObject, Vector3.zero, 0.5f);
    }
}
