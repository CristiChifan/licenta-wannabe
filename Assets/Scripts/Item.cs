using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEditor.Progress;

public class Item : MonoBehaviour
{
    private SpriteRenderer _renderer;


    public void Awake()
    {
        _renderer = GetComponent<SpriteRenderer>();
        ChooseSprite();
    }

    public void ChooseSprite()
    {
        Sprite[] sprites = Resources.LoadAll<Sprite>("Sprites/Items");
        _renderer.sprite = sprites[Random.Range(0, sprites.Length + 1)];
    }


    public void ChooseRandomEffect(List<Character> characters)
    {
        int randomNum = Random.Range(0, 4);
        foreach (Character character in characters)
        {

            if (randomNum == 0)
            {
                character.MaxHealth += 10;
                character.Health += 10;
            }
            else if (randomNum == 1)
            {
                character.Shield += 20;
            }   
            else if (randomNum == 2)
            {
                character.MagicDamage += 2;
            }
            else if (randomNum == 3)
            {
                character.PhysicalDamage += 2;
            }
        }
    }
}
