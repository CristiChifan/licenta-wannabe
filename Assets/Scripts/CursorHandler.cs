using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorHandler : MonoBehaviour
{

    public Texture2D cursor_normal;
    public Vector2 normalCursorHotSpot;

    public Texture2D cursor_pointer;
    public Vector2 pointerCursorHotSpot;


    public void OnCursorEnter()
    {
        Cursor.SetCursor(cursor_pointer, pointerCursorHotSpot, CursorMode.Auto);
    }

    public void OnCursorExit()
    {
        Cursor.SetCursor(cursor_normal, normalCursorHotSpot, CursorMode.Auto);
    }
}
