using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour
{
    // Start is called before the first frame update

    public Animator transition;
    public float transitionTime = 1f;

    private static int TREASURE_SCENE_INDEX = 5;
    private static int RECRUIT_SCENE_INDEX = 4;
    private static int REST_SCENE_INDEX = 3;
    private static int FIGHT_SCENE_INDEX = 2;
    private static int GAME_SCENE_INDEX = 1;
    void Start()
    {
        transition.gameObject.SetActive(true);
    }

    public void PlayGame()
    {
        StartCoroutine(LoadLevel(GAME_SCENE_INDEX));
    }

    public void GoToCombat()
    {
        StartCoroutine(LoadLevelAdditive(FIGHT_SCENE_INDEX));

    }

    public void GoToRest()
    {
        StartCoroutine(LoadLevelAdditive(REST_SCENE_INDEX));

    }

    public void GoToRecruit()
    {
        StartCoroutine(LoadLevelAdditive(RECRUIT_SCENE_INDEX));
    }

    public void GoToTreasure()
    {
        StartCoroutine(LoadLevelAdditive(TREASURE_SCENE_INDEX));
    }

    public void ReturnToMapFromRecruit()
    {
        StartCoroutine(ReturnToGameFromScene(RECRUIT_SCENE_INDEX));
    }


    public void ReturnToMapFromFight()
    {
        StartCoroutine(ReturnToGameFromScene(FIGHT_SCENE_INDEX));
    }

    public void ReturnToMapFromRest()
    {
        StartCoroutine(ReturnToGameFromScene(REST_SCENE_INDEX));
    }

    public void ReturnToMapFromTreasure()
    {
        StartCoroutine(ReturnToGameFromScene(TREASURE_SCENE_INDEX));
    }

    private IEnumerator ReturnToGameFromScene(int index)
    {
        transition.gameObject.SetActive(true);
        transition.SetTrigger("Start");
        yield return new WaitForSeconds(transitionTime);
        SceneManager.UnloadSceneAsync(index);
        SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(GAME_SCENE_INDEX));
        ReEnableMap();
        transition.gameObject.SetActive(false);

    }

    public void ReEnableMap()
    {
        var gridManager = GameObject.Find("GridManager").GetComponent<GridManager>();
        gridManager._background.gameObject.SetActive(true);
        gridManager._gridParent.gameObject.SetActive(true);
        gridManager._realLine.gameObject.SetActive(true);
        gridManager._ui.gameObject.SetActive(true);
    }

    private IEnumerator LoadLevel(int levelIndex)
    {
        transition.SetTrigger("Start");
        yield return new WaitForSeconds(transitionTime);

        SceneManager.LoadScene(levelIndex);
    }

    private IEnumerator LoadLevelAdditive(int levelIndex)
    {
        transition.SetTrigger("Start");
        yield return new WaitForSeconds(transitionTime);
        SceneManager.LoadScene(levelIndex, LoadSceneMode.Additive);
        transition.gameObject.SetActive(false);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
