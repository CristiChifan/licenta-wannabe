using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityFactory : MonoBehaviour
{
    readonly List<Ability> abilities = new List<Ability>();
    // Start is called before the first frame update
    public List<Ability> CreateAbilities(CharacterClass characterClass)
    {
        abilities.Clear();
        switch (characterClass)
        {
            case CharacterClass.Rogue:
                CreateRogueAbilities();
                break;
            case CharacterClass.Fighter:
                CreateFighterAbilities();
                break;
            case CharacterClass.Wizard:
                CreateWizardAbilities();
                break;
            case CharacterClass.Healer:
                CreateHealerAbilities();
                break;
            default:
                break;
        }

        return abilities;
    }


    #region methods
    private void CreateRogueAbilities()
    {
        Ability a = new Ability();
        a.Init("Backstab" ,"da damage", 10, Target.SingleEnemy, 0);
        abilities.Add(a);

        Ability b = new Ability();
        b.Init("Evicerate", "da damage dar 10% sansa sa dea mult damage (sau poate in functie de backstab", 10, Target.SingleEnemy, 0);
        abilities.Add(b);

        Ability c = new Ability();
        c.Init("Dodge", "shield in plus", 10, Target.Self, 0, DamageType.Sustain, true);
        abilities.Add(c);

        Ability d = new Ability();
        d.Init("Caltrops", "damage putin la toti", 10, Target.AllEnemies, 0);
        abilities.Add(d);
    }

    private void CreateFighterAbilities()
    {
        Ability a = new Ability();
        a.Init("Slash", "da damage", 10, Target.SingleEnemy, 0);
        abilities.Add(a);

        Ability b = new Ability();
        b.Init("Second Wind", "heal solo", 10, Target.Self, 0);
        abilities.Add(b);

        Ability c = new Ability();
        c.Init("Protect", "shield la ally", 10, Target.SingleAlly, 1, DamageType.Sustain, true);
        abilities.Add(c);

        Ability d = new Ability();
        d.Init("Whirlwind", "dmg la toti da in range gen 1-3 damage", 10, Target.AllEnemies, 0);
        abilities.Add(d);
    }

    private void CreateWizardAbilities()
    {
        Ability a = new Ability();
        a.Init("Firebolt", "damage", 10, Target.SingleEnemy, 0);
        abilities.Add(a);

        Ability b = new Ability();
        b.Init("Arcane Missiles", "damage la toti da intre 4-6", 10, Target.AllEnemies, 0);
        abilities.Add(b);

        Ability c = new Ability();
        c.Init("Arcane Blast", "50% sansa sa dea mult 50% sa dea putin", 10, Target.SingleEnemy, 0);
        abilities.Add(c);

        Ability d = new Ability();
        d.Init("Witch Bolt", "range mare de damage gen 1-50 dar 10 recoil damage", 10, Target.SingleEnemy, 0);
        abilities.Add(d);
    }

    private void CreateHealerAbilities()
    {
        Ability a = new Ability();
        a.Init("Healing Word", "heal", 10, Target.SingleAlly, 0);
        abilities.Add(a);

        Ability b = new Ability();
        b.Init("Aura of Life", "heal over time pt 2 ture", 10, Target.SingleAlly, 2);
        abilities.Add(b);

        Ability c = new Ability();
        c.Init("Healing Surge", "heal putin la toti", 10, Target.AllAllies, 0 );
        abilities.Add(c);

        Ability d = new Ability();
        d.Init("Bless", "shield la toti pe mai multe ture", 10, Target.SingleAlly, 2);
        abilities.Add(d);
    }
    #endregion
}
