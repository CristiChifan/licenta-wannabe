using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using static UnityEditor.Progress;


public enum Dificulty
{
    Easy,
    Normal,
    Hard
}

public class PlayerHandler : MonoBehaviour
{
    [SerializeField] private Dificulty _dificulty = Dificulty.Easy;
    [SerializeField] private Character _characterPrefab;
    [SerializeField] private Item _itemPrefab;


    public static PlayerHandler instance;
    private GridManager _gridManager;
    private List<Character> _characters = new List<Character>();
    private List<Item> _items = new List<Item>();
    private Sprite unpressedButton, pressedButton;
    private int size, itemCounter = 0;
    public TileType currentFightTileType;

    #region gGetters&Setters
    public Dificulty Dificulty { get { return _dificulty; } set { _dificulty = value; } }

    public List<Character> Characters { get { return _characters; } set { _characters = value; } }

    public List<Item> Items { get { return _items; } set { _items = value; } }
    #endregion


    #region Methods

    //singleton object
    public void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
        if (instance == null)
        {

            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void Start()
    {
        unpressedButton = Resources.Load<Sprite>("Sprites/UI/PNG/buttonSquare_brown");
        pressedButton = Resources.Load<Sprite>("Sprites/UI/PNG/buttonSquare_brown_pressed");
    }

    public void AddCharacter(CharacterClass characterClass, string name = null)
    {
        Character character = Instantiate(_characterPrefab, new Vector3(0, 0, 0), Quaternion.identity);
        character.gameObject.SetActive(false);
        character.Init(characterClass);
        if(name != null)
        {
            character.Name = name;
        }

        Characters.Add(character);
        UpdateCharacterUI();
    }

    public void AddItem(Item item)
    {
        Debug.Log(item.GetComponent<SpriteRenderer>().sprite.name);
        Item newItem = Instantiate(item, new Vector3(0, 0, 0), Quaternion.identity);
        newItem.gameObject.SetActive(false);
        Items.Add(newItem);
    }

    public void SetDificulty()
    {
        _gridManager = GameObject.Find("GridManager").GetComponent<GridManager>();
        if (_gridManager != null)
        {
            switch (_dificulty)
            {
                case Dificulty.Easy:
                    size = 3;
                    break;
                case Dificulty.Normal:
                    size = 5;
                    break;
                case Dificulty.Hard:
                    size = 7;
                    break;
                default:
                    size = 3;
                    break;
            }

            _gridManager.DestroyOldGrid();
            _gridManager.Size = size;
            _gridManager.GenerateGrid();
            _gridManager.UpdateCameraPosition();
        }

    }

    public void SetDificultyOnButton()
    {
        string name = EventSystem.current.currentSelectedGameObject.name;
        var easyButton = GameObject.Find("EasyButton").GetComponent<Image>();
        var mediumButton = GameObject.Find("MediumButton").GetComponent<Image>();
        var hardButton = GameObject.Find("HardButton").GetComponent<Image>();

        if (name == "EasyButton")
        {
            _dificulty = Dificulty.Easy;
            easyButton.sprite = pressedButton;
            mediumButton.sprite = unpressedButton;
            hardButton.sprite = unpressedButton;
        }
        else if (name == "MediumButton")
        {
            _dificulty = Dificulty.Normal;
            easyButton.sprite = unpressedButton;
            mediumButton.sprite = pressedButton;
            hardButton.sprite = unpressedButton;
        }
        else if (name == "HardButton")
        {
            _dificulty = Dificulty.Hard;
            easyButton.sprite = unpressedButton;
            mediumButton.sprite = unpressedButton;
            hardButton.sprite = pressedButton;
        }
    }


    public void UpdateCharacterUI()
    {
        var gridManager = GameObject.Find("GridManager").GetComponent<GridManager>();
        for (int i = 0; i < Characters.Count; i++)
        {
            var canvas = gridManager._ui.Find("Canvas").GetComponent<Canvas>();
            //enable the sprite
            Image image = canvas.transform.Find($"Character{i}").GetComponent<Image>();
            image.sprite = Characters[i].GetComponent<SpriteRenderer>().sprite;
            image.enabled = true;

            //enable the name of the sprite
            TextMeshProUGUI name = canvas.transform.Find($"Name{i}").GetComponent<TextMeshProUGUI>();
            name.text = Characters[i].Name;

            //enable status bars
            Slider hSlider = canvas.transform.Find($"HealthBar{i}").GetComponent<Slider>();
            Slider sSlider = canvas.transform.Find($"ShieldBar{i}").GetComponent<Slider>();
            hSlider.value = Characters[i].Health / Characters[i].MaxHealth;
            sSlider.value = 0;
            Debug.Log(Characters[i].Health);
            Debug.Log(hSlider.value);
        }
    }


    public void UpdateItemUI(Sprite sprite)
    {
        var gridManager = GameObject.Find("GridManager").GetComponent<GridManager>();
        var canvas = gridManager._ui.Find("Canvas").GetComponent<Canvas>();
        Image image = canvas.transform.Find($"Item{itemCounter}").GetComponent<Image>();
        image.sprite = sprite;
        image.enabled = true;
        itemCounter++;
    }

    #endregion


}
