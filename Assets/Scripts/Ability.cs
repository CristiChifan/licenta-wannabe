using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Target
{
    SingleAlly,
    SingleEnemy,
    AllAllies,
    AllEnemies,
    Self
}
public enum DamageType
{
    Phisycal,
    Magical,
    Sustain
}

// DACA DURATION = 0 ATUNCI ABILITATEA MERGE FIX IN ACEL MOMENT
// DACA E > 1 ATUNCI SE APLICA SI PE TURELE CARE URMEAZA

public class Ability : MonoBehaviour
{
    private string _name;
    private string _text;
    private float _effect;
    private Target _target;
    private int _duration;
    public bool isShield = false;
    private DamageType damageType;

    public void Init(string _name, string _text, float _effect, Target _target, int _duration, DamageType damageType = DamageType.Sustain, bool _isShield = false)
    {
        Name = _name;
        Text = _text;
        Effect = _effect;
        Target = _target;
        Duration = _duration;
        isShield = _isShield;
        DamageType = damageType;
    }


    #region Getters&Setters

    public string Name { get { return _name; } set { _name = value; } }
    public string Text { get { return _text; } set { _text = value; } }

    public float Effect { get { return _effect; } set { _effect = value; } }

    public Target Target { get { return _target; } set { _target = value; } }

    public DamageType DamageType { get { return damageType; } set { damageType = value; } }

    public int Duration { get { return _duration; } set { _duration = value; } }

    #endregion
}
