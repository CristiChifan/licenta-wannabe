using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BansheeGz.BGSpline.Curve;



public class GridManager : MonoBehaviour
{
    [SerializeField] public GameObject _background;
    [SerializeField] private Tile _defaultTile;
    [SerializeField] private Camera _camera;
    [SerializeField] public int _size = 3;
    [SerializeField] private BGCurve _path;
    [SerializeField] private PlayerHandler _playerHandler;
    [SerializeField] public Transform _gridParent;
    [SerializeField] public Transform _ui;
    [SerializeField] public GameObject _gameOverOverlay;
    [SerializeField] public GameObject _goodGameOverOverlay;
    [SerializeField] public AbilityFactory abilityFactory;

    private List<List<Tile>> _tiles = new List<List<Tile>>();
    private List<Tile> _visitedTiles = new List<Tile>();
    public BGCurve _realLine;
    private float rows, cols;
    private bool lineCreated = false;
    public static GridManager instance;
    public bool flipper = false;
    public float finalYValue;

    public void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        _playerHandler = GameObject.Find("PlayerHandler").GetComponent<PlayerHandler>();
        _playerHandler.SetDificulty();
        GameObject.Find("PlayerHandler").GetComponent<PlayerHandler>().AddCharacter(CharacterClass.Fighter);
    }

    // generate a hexagone of nodes with the side's size equal to 3 or more nodes 
    public void GenerateGrid()
    {
        _size = _size < 3 ? 3 : _size;
        rows = 2 * _size - 1;
        float prev = 2;
        if (!lineCreated)
        {
            _realLine = Instantiate(_path, new Vector3(0, 0, 5), Quaternion.identity);
            _realLine.name = "Path";
            _realLine.Apply2D(BGCurve.Mode2DEnum.XY);
            //line.transform.position = new Vector3();
            lineCreated = true;
        }
        for (int x = 0; x < rows; x++)
        {
            _tiles.Add(new List<Tile>());

            // make the first row always available
            _defaultTile.IsAvailable = x == 0;

            //generate a hexagon shaped grid
            cols = x < _size ?  x + _size : prev;
            for (int y = 0; y < cols; y++)
            {
                prev = y;
                float posX = (rows - cols) / 2f + y;
                float posY = x;
                var newTile = Instantiate(_defaultTile, new Vector3(posX, posY, 0), Quaternion.identity, _gridParent);
                newTile.Init(new KeyValuePair<int, int>(x, y), new BGCurvePoint(_realLine,
                new Vector3(posX + 0.6f, posY + 0.6f, 0), BGCurvePoint.ControlTypeEnum.BezierSymmetrical));
                _tiles[x].Add(newTile);
                finalYValue = posY;
            }
        }
    }

    #region getters&setters
    public int Size { get { return _size; } set { _size = value; } }

    #endregion


    #region methods

    public void UpdateCameraPosition()
    {
        _camera.transform.position = new Vector3(rows / 2, rows / 2, -10);
        _background.transform.position = new Vector3(rows / 2, rows / 2, 0);

        _camera.orthographicSize = _size + 1;
        _background.transform.localScale = new Vector3(_size * 5 + 3, _size * 6, 0);

    }

    public void DestroyOldGrid()
    {
        foreach (var row in _tiles.ToArray())
        {
            foreach (var tile in row.ToArray())
            {
                Destroy(tile.gameObject);
                row.Remove(tile);
            }
        }

        foreach (var row in _tiles.ToArray())
        {
            _tiles.Remove(row);
        }

        _path.Clear();

    }

    public void DisableTileRow(int row)
    {
        foreach (var tile in _tiles[row])
        {
            tile.Disable();
        }
    }

    public void EnableNextTiles(Tile curentTile)
    {
        int first = curentTile.Coords.Value;
        int second = curentTile.Coords.Key + 1 < _size? first + 1 : first - 1;
        if(curentTile.Coords.Key < _size * 2 - 2)
        {
            if(first < _tiles[curentTile.Coords.Key + 1].Count)
            {
                _tiles[curentTile.Coords.Key + 1][first].IsAvailable = true;
            }
            if (second >= 0)
            {
                _tiles[curentTile.Coords.Key + 1][second].IsAvailable = true;
            }
        }
    }

    public void DecideCurrent(Tile currentTile)
    {
        
        if (_visitedTiles.Count > 0)
        {
            foreach (var tile in _visitedTiles)
            {
                tile.IsCurrent = false;
                tile.IsVisied = true;
                tile.ApplyVisitedMarker();
            }
        }
        _visitedTiles.Add(currentTile);
        currentTile.IsCurrent = true;
        DisableTileRow(currentTile.Coords.Key);
        EnableNextTiles(currentTile);
        CreateLine(currentTile);
        LevelLoader levelLoader = GameObject.Find("LevelLoader").GetComponent<LevelLoader>();

        TileType currentType = currentTile.TileType;

        if (currentType == TileType.Enemy)
        {
            _playerHandler.currentFightTileType = TileType.Enemy;
            levelLoader.GoToCombat();
            DisableMap();
        } 
        else if (currentType == TileType.Rest)
        {
            levelLoader.GoToRest();
            DisableMap();
        }
        else if (currentType == TileType.Recruit)
        {
            levelLoader.GoToRecruit();
            DisableMap();
        } 
        else if (currentType == TileType.Treasure)
        {
            levelLoader.GoToTreasure();
            DisableMap();
        }
        else if (currentType == TileType.EpicEnemy)
        {
            _playerHandler.currentFightTileType = TileType.EpicEnemy;
            levelLoader.GoToCombat();
            DisableMap();
        }
        else if (currentType == TileType.Boss)
        {
            _playerHandler.currentFightTileType = TileType.Boss;
            levelLoader.GoToCombat();
            DisableMap();
        }
        else if (currentType == TileType.Empty && currentTile.Coords.Key == finalYValue)
        {
            _gameOverOverlay.gameObject.SetActive(true);
        }

    }

    public void DisableMap()
    {
        _gridParent.gameObject.SetActive(false);
        _background.gameObject.SetActive(false);
        _realLine.gameObject.SetActive(false);
        _ui.gameObject.SetActive(false);
    }

    private void CreateLine(Tile currentTile)
    {
        if (_realLine.PointsCount > 0)
        {
            Vector3 positionEnd = currentTile.Point.PositionLocal;
            Vector3 positionStart = _realLine.Points[_realLine.PointsCount - 1].PositionLocal;

            Vector3 positionMid = (positionEnd + positionStart) / 2;
            if (flipper) 
            {
                positionMid.x += Random.Range(0.2f, 0.6f);
            } else
            {
                positionMid.x -= Random.Range(0.2f, 0.6f);
            }
            BGCurvePoint curvePoint = new BGCurvePoint(currentTile.Point.Curve, positionMid);
            _realLine.AddPoint(curvePoint);
            _realLine.AddPoint(currentTile.Point);
        } else
        {
            _realLine.AddPoint(currentTile.Point);
        }

        _realLine.Apply2D(BGCurve.Mode2DEnum.XY);
        _realLine.GetComponent<Renderer>().sharedMaterial.SetFloat("_Tiling", (_visitedTiles.Count - 1) * 2);
        flipper = !flipper;
    }


    #endregion
}
    
