using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;
public enum CharacterClass
{
    Rogue,
    Fighter,
    Wizard,
    Healer
};

public class Character : MonoBehaviour
{

    private float _hp, _speed, _magicDamage, _physicalDamage, _shield;
    private float maxHealth = 100, maxShield = 100;
    [SerializeField] private string _name = "Bob";
    [SerializeField] private CharacterClass _characterClass;
    [SerializeField] private AbilityFactory abilityFactory;

    

    

    public UnityEvent<float> OnHealthChange;
    public UnityEvent<float> OnShieldChange;

    // o sa avem mereu doar 4;
    private List<Ability> abilities = new List<Ability>();
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }


    #region Getters&Setters

    public string Name { get { return _name; } set { _name = value; } }

    public CharacterClass CharacterClass { get { return _characterClass; } set { _characterClass = value; } }

    public float Health
    {
        get { return _hp; }
        set
        {
            _hp = value;
            OnHealthChange?.Invoke(Health / maxHealth);
        }
    }

    public float Speed { get { return _speed; } set { _speed = value; } }

    public float MaxHealth { get { return maxHealth; } set { maxHealth = value; } }

    public float MagicDamage { get { return _magicDamage; } set { _magicDamage = value; } }

    public float PhysicalDamage { get { return _physicalDamage; } set { _physicalDamage = value; } }

    public float Shield
    {
        get { return _shield; }
        set
        {
            _shield = value;
            OnShieldChange?.Invoke(Shield / maxShield);
        }
    }

    public List<Ability> Abilities { get { return abilities; } set { abilities = value; } }

    #endregion


    #region methods
    public void Init(CharacterClass characterClass)
    {
        CharacterClass = characterClass;
        ChooseSprite(_characterClass);
        Name = ChooseRandomName();
        ChooseRandomSpeed();
        abilities.AddRange(abilityFactory.CreateAbilities(characterClass));
        GetComponent<SpriteRenderer>().sortingOrder = 10;
        GetComponentInChildren<Canvas>().sortingOrder = 10;
        Health = maxHealth;
        Shield = 0;
        Canvas canvas = this.GetComponentInChildren<Canvas>();
        TextMeshProUGUI name = canvas.GetComponentInChildren<TextMeshProUGUI>();
        name.text = Name;
    }

    public string ChooseRandomName()
    {
        string[] names = { "Aegaeon", "Phorcys", "Cricios", "Bronze", "Heabek", "Heaving", "Sunill", "Bephel", "Dundee", "Kishiko", "Chloris", "Thelxi", "Epione", "Crowtail", "Dawncrest", "Lunar", "Rainail", "Chloe", "Buzby", "Holly", "Zarri", "Catok", "Grurda", "Buttons", "Mayfly", "Ghost", "Nordela", "Ant", "Fangs", "Grimm" };

        return names[Random.Range(0, names.Length - 1)];
    }

    public void ChooseRandomSpeed()
    {
        _speed = Random.Range(0.1f, 1f);
    }

    public void ChooseSprite(CharacterClass characterClass)
    {
        SpriteRenderer _renderer = GetComponent<SpriteRenderer>();

        if (characterClass == CharacterClass.Rogue)
        {
            _renderer.sprite = Resources.Load<Sprite>("Sprites/Characters/figure_180x180_framed_standard_18_rogue");
        } 
        else if (characterClass == CharacterClass.Fighter)
        {
            _renderer.sprite = Resources.Load<Sprite>("Sprites/Characters/figure_180x180_framed_standard_6_hero");
        }
        else if (characterClass == CharacterClass.Wizard)
        {
            _renderer.sprite = Resources.Load<Sprite>("Sprites/Characters/figure_180x180_framed_standard_15_mage");
        }
        else if (characterClass == CharacterClass.Healer)
        {
            _renderer.sprite = Resources.Load<Sprite>("Sprites/Characters/figure_180x180_framed_standard_8_priest");
        }
    }
    #endregion
}

