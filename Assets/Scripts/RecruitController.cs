using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class RecruitController : MonoBehaviour
{

    [SerializeField] public Character _characterPrefab;
    [SerializeField] public Button recruitButton;

    private Character newCharacter;
    private PlayerHandler _playerHandler;
    private static int RECRUIT_SCENE_INDEX = 4;
    private static int GAME_SCENE_INDEX = 1;



    private CharacterClass newCharacterClass;

    // Start is called before the first frame update
    void Start()
    {
        _playerHandler = GameObject.Find("PlayerHandler").GetComponent<PlayerHandler>();

        DecideNewCharacter();
        SetupCurrentCharacters();
        if (_playerHandler.Characters.Count < 4)
        {
            SetupNewCharacter();
        } 
        else 
        {
            ChangeRecruitButtonText();
        }
    }

    public void SetupCurrentCharacters()
    {
        SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(RECRUIT_SCENE_INDEX));
        for (int i = 0; i < _playerHandler.Characters.Count; i++)
        {
            var _clone = Instantiate(_playerHandler.Characters[i], new Vector3(4 + 1.3f * i, -3.5f, 0), Quaternion.identity);
            _clone.GetComponent<SpriteRenderer>().flipX = false;
            _clone.gameObject.SetActive(true);
        }
    }

    public void SetupNewCharacter()
    {
        newCharacter = Instantiate(_characterPrefab, new Vector3(-3.5f, -3.5f, 0), Quaternion.identity);
        newCharacter.CharacterClass = newCharacterClass;
        newCharacter.Init(newCharacter.CharacterClass);
        SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(GAME_SCENE_INDEX));
    }

    public void ChangeRecruitButtonText()
    {
        var text = recruitButton.GetComponentInChildren<TextMeshProUGUI>();

        text.text = "No more friends to recruit!";
    }

    public void DecideNewCharacter()
    {

        if (_playerHandler.Characters.Count == 1)
        {
            newCharacterClass = CharacterClass.Wizard;
        }
        else if (_playerHandler.Characters.Count == 2)
        {
            newCharacterClass = CharacterClass.Rogue;
        }
        else if (_playerHandler.Characters.Count == 3)
        {
            newCharacterClass = CharacterClass.Healer;
        }

    }

    public void AddCharacter()
    {
        StartCoroutine(AddCharacterRoutine());
    }


    IEnumerator AddCharacterRoutine()
    {

        if (_playerHandler.Characters.Count < 4)
        {
            LeanTween.moveLocalX(newCharacter.gameObject, 4 + 1.3f * _playerHandler.Characters.Count, 1).setEaseInBack();
            yield return new WaitForSeconds(1.5f);
            newCharacter.GetComponent<SpriteRenderer>().flipX = false;
            _playerHandler.AddCharacter(newCharacterClass, newCharacter.Name);
        } else
        {
            yield return null;
        }
        GameObject.Find("LevelLoader").GetComponent<LevelLoader>().ReturnToMapFromRecruit();

    }
}
